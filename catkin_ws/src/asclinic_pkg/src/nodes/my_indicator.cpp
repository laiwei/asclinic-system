// Copyright (C) 2021, The University of Melbourne, Department of Electrical and Electronic Engineering (EEE)
//
// This file is part of ASClinic-System.
//    
// See the root of the repository for license details.
//
// ----------------------------------------------------------------------------
//     _    ____   ____ _ _       _          ____            _                 
//    / \  / ___| / ___| (_)____ (_) ___    / ___| _   _ ___| |_ ___ ________  
//   / _ \ \___ \| |   | | |  _ \| |/ __|___\___ \| | | / __| __/ _ \  _   _ \ 
//  / ___ \ ___) | |___| | | | | | | (_|_____|__) | |_| \__ \ ||  __/ | | | | |
// /_/   \_\____/ \____|_|_|_| |_|_|\___|   |____/ \__, |___/\__\___|_| |_| |_|
//                                                 |___/                       
//
// DESCRIPTION:
// Template node for polling the value of a GPIO pin at a fixed frequency
//
// ----------------------------------------------------------------------------





#include "ros/ros.h"
#include <ros/package.h>
#include "std_msgs/Float32.h"

#include <gpiod.h>


float light_duration = 0;


// Respond to subscriber receiving a message
void setIndicatorCallback(const std_msgs::Float32 & msg)
{
	light_duration = msg.data;
}

int main(int argc, char* argv[])
{
	// Initialise the node
	ros::init(argc, argv, "my_indicator");
	ros::NodeHandle node_handle_for_global("/my_global_namespace");

	ros::Subscriber set_indicator_subscriber = node_handle_for_global.subscribe("set_indicator_second", 1, setIndicatorCallback);


	const char * gpio_chip_name = "/dev/gpiochip0";

	int line_number = 157;

	// Initialise a GPIO chip, line, and event object
	struct gpiod_chip *chip;
	struct gpiod_line *line;

	int value;
	value = gpiod_ctxless_get_value(gpio_chip_name, line_number, false, "foobar");

	chip = gpiod_chip_open(gpio_chip_name);
	// Retrieve the GPIO line
	line = gpiod_chip_get_line(chip,line_number);
	// Display the status

    gpiod_line_request_output(line, "gpio", 0);

	// Enter a loop that continues while ROS is still running
	while (ros::ok())
	{
		if(light_duration){
	        int ret = gpiod_line_set_value(line, 0);
			ros::Duration(light_duration).sleep();
			ret = gpiod_line_set_value(line, 1);
			ros::Duration(light_duration).sleep();
		}
		ros::spinOnce();
	} // END OF: "while (ros::ok())"

	// Close the GPIO chip
	gpiod_chip_close(chip);

	return 0;
}
