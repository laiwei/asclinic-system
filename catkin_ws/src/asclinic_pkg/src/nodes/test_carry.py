#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import rospy
from std_msgs.msg import UInt32
from std_msgs.msg import Float32
# Import the asclinic message types
from asclinic_pkg.msg import PathInstruction
import my_path_planning as mp
import my_pos_estimate as my_pos

class Test:

    def __init__(self):
       
        # Initialise a publisher
        self.set_path_instruction = rospy.Publisher("/my_global_namespace"+"/get_path_instruction", PathInstruction, queue_size=10)
        # self.set_blink = rospy.Publisher("/my_global_namespace"+"/set_indicator_second", Float32, queue_size=10)
        self.set_current_theta = rospy.Publisher("/my_global_namespace"+"/set_current_theta", Float32, queue_size=10)
        
        # # Initialise a subscriber
        # rospy.Subscriber("/my_global_namespace"+"/set_instruction_done", UInt32, self.myInstructionDoneCallback)
        rospy.sleep(2.0)
        self.run()



    # Respond to timer callback
    def run(self):
        pos = my_pos.get_current_pos()
        print(pos)
        theta = -pos[3]
        path_instruction = PathInstruction()
        path_instruction.isRelative = 1
        path_instruction.theta = int(theta)
        path_instruction.dis = 0
        self.set_path_instruction.publish(path_instruction)


if __name__ == '__main__':
    # Initialise the node
    global node_name
    node_name = "Test"
    rospy.init_node(node_name)
    template_py_node = Test()
    # Spin as a single-threaded node
    rospy.spin()
