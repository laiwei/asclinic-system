#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import rospy
from std_msgs.msg import UInt32
from std_msgs.msg import Float32
# Import the asclinic message types
from asclinic_pkg.msg import MotorDriveControl
from asclinic_pkg.msg import StartRobot
import my_path_planning as mp
import my_pos_estimate as my_pos
from std_msgs.msg import UInt16

class myReachLoad:

    def __init__(self):
       
        self.isRunning = False
        self.isUp = 1
        self.ins_num = 0

        # Initialise a publisher
        self.motor_drive_control = rospy.Publisher("/my_global_namespace"+"/set_motor_duty_cycle", MotorDriveControl, queue_size=10)
        self.template_publisher = rospy.Publisher("/my_global_namespace"+"/set_servo_pulse_width", UInt16, queue_size=10)
        self.start_robot = rospy.Publisher("/my_global_namespace"+"/Start_Robot", StartRobot, queue_size=10)

        rospy.Subscriber("/my_global_namespace"+"/reach_to_object", StartRobot, self.reachToObjectCallBack)
        rospy.Subscriber("/my_global_namespace"+"/tof_distance", UInt16, self.tofDistaceCallBack)


        # # Initialise a subscriber
        # rospy.Subscriber("/my_global_namespace"+"/set_instruction_done", UInt32, self.myInstructionDoneCallback)
        rospy.sleep(2.0)

        start_msg = StartRobot()
        start_msg.first_dist = 300
        start_msg.first_theta = 0
        start_msg.target_theta = -90
        start_msg.target_id = 10
        start_msg.isUp = 1
        self.start_robot.publish(start_msg)

        

    def tofDistaceCallBack(self,msg):
        distance = msg.data
        dis_th = 300

        if(self.isUp):
            dis_th = 380

        # print('robot stopped:',distance, 'thres:',dis_th,'isUp:',self.isUp)

        if(distance < dis_th and self.isRunning):
            motor_msg = MotorDriveControl()
            motor_msg.dir = 5
            motor_msg.speed = 0
            motor_msg.lspeed = 0
            motor_msg.rspeed = 0
            self.motor_drive_control.publish(motor_msg)
            self.isRunning = False

        
       

    def reachToObjectCallBack(self,msg):
        print('get the instruction')
        self.target_theta = msg.target_theta
        self.target_id = msg.target_id
        self.isUp = msg.isUp
        self.run()


    # Respond to timer callback
    def run(self):
        
        id = self.target_id
        theta = self.target_theta

        self.isRunning = True

        

        while(1):

            if( not self.isRunning):
                print('enter not true')
                break

            pos,theta = my_pos.get_t_vec(id)
            x = pos[0]
            motor_msg = MotorDriveControl()

            counter = 0

            print('relative pos:',pos,'theta',theta)

            if(x != -100):
                counter = 0
                if(x<0 and x>-0.1):
                    if(theta < theta-1):
                        motor_msg.dir = 5
                        motor_msg.speed = 0
                        motor_msg.lspeed = 17
                        motor_msg.rspeed = -15
                        self.motor_drive_control.publish(motor_msg)
                    else:
                        if(x<0 and x>-0.05):
                            motor_msg.dir = 5
                            motor_msg.speed = 0
                            motor_msg.lspeed = 15
                            motor_msg.rspeed = -15
                            self.motor_drive_control.publish(motor_msg)
                        else:
                            motor_msg.dir = 5
                            motor_msg.speed = 0
                            motor_msg.lspeed = 15
                            motor_msg.rspeed = -17
                            self.motor_drive_control.publish(motor_msg)
                elif ( x>=0 and x<0.1):
                    if(theta > theta+1):
                        motor_msg.dir = 5
                        motor_msg.speed = 0
                        motor_msg.lspeed = 15
                        motor_msg.rspeed = -17
                        self.motor_drive_control.publish(motor_msg)
                    else:
                        if(x >= 0 and x<0.05):
                            motor_msg.dir = 5
                            motor_msg.speed = 0
                            motor_msg.lspeed = 15
                            motor_msg.rspeed = -15
                            self.motor_drive_control.publish(motor_msg)
                        else:
                            motor_msg.dir = 5
                            motor_msg.speed = 0
                            motor_msg.lspeed = 17
                            motor_msg.rspeed = -15
                            self.motor_drive_control.publish(motor_msg)
                elif ( x<=-0.1):
                    motor_msg.dir = 5
                    motor_msg.speed = 0
                    motor_msg.lspeed = 15
                    motor_msg.rspeed = -17
                    self.motor_drive_control.publish(motor_msg)
                elif ( x>=0.1):
                    motor_msg.dir = 5
                    motor_msg.speed = 0
                    motor_msg.lspeed = 17
                    motor_msg.rspeed = -15
                    self.motor_drive_control.publish(motor_msg)
                # print(pos, x, theta)
                rospy.sleep(3.0)
                motor_msg.dir = 5
                motor_msg.speed = 0
                motor_msg.lspeed = 0
                motor_msg.rspeed = 0
                self.motor_drive_control.publish(motor_msg)
            else:
                
                counter = counter + 1

                if(counter > 5):
                    motor_msg.dir = 5
                    motor_msg.speed = 0
                    motor_msg.lspeed = -20
                    motor_msg.rspeed = 20
                    self.motor_drive_control.publish(motor_msg)
                    rospy.sleep(1.5)
                    motor_msg.dir = 5
                    motor_msg.speed = 0
                    motor_msg.lspeed = 0
                    motor_msg.rspeed = 0
                    self.motor_drive_control.publish(motor_msg)

                    counter = 0
        
        
        print('reached to the object',self.isRunning)
        if(self.isUp):
            msg = UInt16()
            msg.data = 1
            self.template_publisher.publish(msg)
            rospy.sleep(1.5)
            msg.data = 0
            self.template_publisher.publish(msg)
        else:
            msg = UInt16()
            msg.data = 2
            self.template_publisher.publish(msg)
            rospy.sleep(1.0)
            msg.data = 0
            self.template_publisher.publish(msg)

        motor_msg.dir = 5
        motor_msg.speed = 0
        motor_msg.lspeed = -20
        motor_msg.rspeed = 20
        self.motor_drive_control.publish(motor_msg)
        rospy.sleep(3.0)
        motor_msg.dir = 5
        motor_msg.speed = 0
        motor_msg.lspeed = 0
        motor_msg.rspeed = 0
        self.motor_drive_control.publish(motor_msg)

        if(self.ins_num == 0):
            rospy.sleep(1.0)
            start_msg = StartRobot()
            start_msg.target_theta = -180
            start_msg.target_id = 17
            start_msg.first_dist = 1200
            start_msg.first_theta = 90
            start_msg.isUp = 0
            self.start_robot.publish(start_msg)
            self.ins_num = self.ins_num + 1
        # Second Load
        elif(self.ins_num == 1): 
            rospy.sleep(1.0)
            start_msg = StartRobot()
            start_msg.target_theta = 0
            start_msg.target_id = 6
            start_msg.first_dist = 500
            start_msg.first_theta = 0
            start_msg.isUp = 1
            self.start_robot.publish(start_msg)
            self.ins_num = self.ins_num + 1
        elif(self.ins_num == 2):
            rospy.sleep(1.0)
            start_msg = StartRobot()
            start_msg.target_theta = -180
            start_msg.target_id = 17
            start_msg.first_dist = 500
            start_msg.first_theta = -180
            start_msg.isUp = 0
            self.start_robot.publish(start_msg)
            self.ins_num = self.ins_num + 1
            # Third Load
        elif(self.ins_num == 3): 
            rospy.sleep(1.0)
            start_msg = StartRobot()
            start_msg.target_theta = 0
            start_msg.target_id = 5
            start_msg.first_dist = 800
            start_msg.first_theta = 90
            start_msg.isUp = 1
            self.start_robot.publish(start_msg)
            self.ins_num = self.ins_num + 1
        elif(self.ins_num == 4):
            rospy.sleep(1.0)
            start_msg = StartRobot()
            start_msg.target_theta = -180
            start_msg.target_id = 17
            start_msg.first_dist = 500
            start_msg.first_theta = -90
            start_msg.isUp = 0
            self.start_robot.publish(start_msg)
            self.ins_num = self.ins_num + 1





if __name__ == '__main__':
    # Initialise the node
    global node_name
    node_name = "my_reach_load"
    rospy.init_node(node_name)
    template_py_node = myReachLoad()
    # Spin as a single-threaded node
    rospy.spin()
