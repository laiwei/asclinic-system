#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import rospy
from std_msgs.msg import UInt32
from std_msgs.msg import Float32
# Import the asclinic message types
from asclinic_pkg.msg import PathInstruction
from asclinic_pkg.msg import MotorDriveControl
from asclinic_pkg.msg import StartRobot
import my_path_planning as mp
import my_pos_estimate as my_pos

class myMainControl:

    def __init__(self):
       

        # self.path_discription = [[-90,100],[0,100]]

        self.path_start_line = 0
        self.path_start_col = 0
        rospy.loginfo("This is main control")
        self.global_instruction_flag = 0

        # Initialise a publisher
        self.set_path_instruction = rospy.Publisher("/my_global_namespace"+"/get_path_instruction", PathInstruction, queue_size=10)
        self.set_blink = rospy.Publisher("/my_global_namespace"+"/set_indicator_second", Float32, queue_size=10)
        self.set_current_theta = rospy.Publisher("/my_global_namespace"+"/set_current_theta", Float32, queue_size=10)
        self.motor_drive_control = rospy.Publisher("/my_global_namespace"+"/set_motor_duty_cycle", MotorDriveControl, queue_size=10)

        self.odometry_timer_control = rospy.Publisher("/my_global_namespace"+"/odometry_timer_control", UInt32, queue_size=10)


        self.reach_to_object = rospy.Publisher("/my_global_namespace"+"/reach_to_object", StartRobot, queue_size=10)
        


        rospy.Subscriber("/my_global_namespace"+"/Start_Robot", StartRobot, self.myStartRobotCallBack)

        # Initialise a subscriber
        rospy.Subscriber("/my_global_namespace"+"/set_instruction_done", UInt32, self.myInstructionDoneCallback)


        # rospy.loginfo("Sleep")
        # blink_time = Float32()
        # blink_time.data = 0.5
        # self.set_blink.publish(blink_time)

        # warehouse_data = [
        # (100  , 100),
        # (3100 , 100),
        # (3100 , 2300),
        # (1500 , 2300),
        # (1500 , 2500),
        # (3100 , 2500),
        # (3100 , 4500),
        # (100  , 4500),
        # (100  , 2500),
        # (900  , 2500),
        # (900  , 2300),
        # (100  , 2300),
        # (100  , 100)]


      
        # self.g = mp.setGraph(warehouse_data,3500,5000,50)
        rospy.loginfo("[my_main_control] Get Graph, waiting for start...")



        


    def myStartRobotCallBack(self, msg):

            # uint32 target_x_pos
            # uint32 target_y_pos
            # uint32 target_id
            # uint32 target_theta 

        self.path_start_line = 0
        self.path_start_col = 0

        
        first_theta = msg.first_theta
        first_dist = msg.first_dist
        self.target_theta = msg.target_theta
        self.target_id = msg.target_id
        self.isUp = msg.isUp


        print("[my_main_control] Start Robot",str(msg))

        counter = 0

        while(1):

            pos = my_pos.get_current_pos()
            if(pos):
                counter = 0
                current_theta =  Float32()
                current_theta.data = pos[3]
                self.set_current_theta.publish(current_theta)

                print('[my_main_control] Publish theta and pos:',pos)

                # self.path_discription = mp.getPath(self.g,(pos[1],pos[2]),(target_x_pos,target_y_pos),3500,5000,50)
                self.path_discription = [[first_theta,first_dist], [self.target_theta,0]]
                rospy.loginfo("[my_main_control] Start")

                # Start Odometry
                odo_msg = UInt32()
                odo_msg.data = 1
                self.odometry_timer_control.publish(odo_msg)


                self.mainController()
                break
            else:
                print('pos not found')
                motor_msg = MotorDriveControl()
                motor_msg.dir = 5
                motor_msg.speed = 0
                motor_msg.lspeed = -20
                motor_msg.rspeed = 20
                self.motor_drive_control.publish(motor_msg)
                rospy.sleep(3.0)
                motor_msg.dir = 5
                motor_msg.speed = 0
                motor_msg.lspeed = 0
                motor_msg.rspeed = 0
                self.motor_drive_control.publish(motor_msg)
            
            



    def mainController(self):
        start_line = self.path_start_line
        start_col = self.path_start_col

        print('test line',start_line, start_col)

        if(start_col):
            # 发布直行指令
            path_instruction = PathInstruction()
            path_instruction.isRelative = 0
            path_instruction.theta = -1
            path_instruction.dis = int(self.path_discription[start_line][start_col])

            self.set_path_instruction.publish(path_instruction)
            rospy.loginfo("Instruction Published: "+str(path_instruction))

        else:
            # 发布转向指令

            pos = my_pos.get_current_pos()
            if(pos):
                current_theta =  Float32()
                current_theta.data = pos[3]
                self.set_current_theta.publish(current_theta)

            path_instruction = PathInstruction()
            path_instruction.isRelative = 0
            path_instruction.theta = int(self.path_discription[start_line][start_col])
            path_instruction.dis = 0

            self.set_path_instruction.publish(path_instruction)
            rospy.loginfo("Instruction Published: "+str(path_instruction))


    def myInstructionDoneCallback(self,msg):

        if(self.path_start_col == 0):
                self.path_start_col = 1
                self.mainController()
                return

        start_line = self.path_start_line + 1
        
        if(start_line < len(self.path_discription)):
            
            
            self.path_start_col = 0
            self.path_start_line = start_line
            self.mainController()
        else:
            print('else')
            self.path_start_line = 0
            self.path_start_col = 0
            print('finish pos 1')
        

            target_msg = StartRobot()
            target_msg.target_id = self.target_id
            target_msg.target_theta = self.target_theta
            target_msg.isUp = self.isUp

            #stop Odometry
            odo_msg = UInt32()
            odo_msg.data = 0
            self.odometry_timer_control.publish(odo_msg)

            self.reach_to_object.publish(target_msg)
            
            



if __name__ == '__main__':
    # Initialise the node
    global node_name
    node_name = "my_main_control"
    rospy.init_node(node_name)
    template_py_node = myMainControl()
    # Spin as a single-threaded node
    rospy.spin()
