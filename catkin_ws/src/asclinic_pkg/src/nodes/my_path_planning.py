#!/usr/bin/env python3
# coding: utf-8


from shapely.geometry import Point
from shapely.geometry.polygon import Polygon
from dijkstar import Graph, find_path
import numpy as np
from math import sqrt


# define functions

def dist(x,y):
    return sqrt((y[0] - x[0])**2 + (y[1]-x[1])**2)

def getPosFromPoint(x_boundary,resolution,point_num):
    x_axis_point_num = x_boundary//resolution
    x = (point_num % x_axis_point_num)*resolution
    y = point_num//x_axis_point_num*resolution
    return x,y

def getPointFromPos(x_boundary,resolution,x,y):
    point = x//resolution + y * x_boundary // resolution // resolution
    return  point

def getRelativePos(x,resolution,boundary,symbol):
    if(symbol == '+'):
        x = x + resolution
    if(symbol == '-'):
        x = x - resolution
    
    if(x > boundary):
        x = np.inf
    if(x < 0):
        x = np.inf
    return x


def setGraph(warehouse_data,x_boundary,y_boundary,resolution):

    warehouse_specification = Polygon(warehouse_data)

    # Construct Distance Matrix
    total_points = int((x_boundary/resolution) * (y_boundary/resolution))
    distance_matrix = [ [np.inf]*total_points for i in range(total_points)]
    
    for i in range(total_points):
        current_x,current_y = getPosFromPoint(x_boundary,resolution,i)
        if (warehouse_specification.intersects(Point(current_x,current_y))):
            top          = (current_x                                          , getRelativePos(current_y,resolution,y_boundary,'+'))
            bottom       = (current_x                                          , getRelativePos(current_y,resolution,y_boundary,'-'))
            left         = (getRelativePos(current_x,resolution,y_boundary,'-'), current_y                                          )
            right        = (getRelativePos(current_x,resolution,y_boundary,'+'), current_y                                          )
            top_left     = (getRelativePos(current_x,resolution,y_boundary,'-'), getRelativePos(current_y,resolution,y_boundary,'+'))
            top_right    = (getRelativePos(current_x,resolution,y_boundary,'+'), getRelativePos(current_y,resolution,y_boundary,'+'))
            bottom_left  = (getRelativePos(current_x,resolution,y_boundary,'-'), getRelativePos(current_y,resolution,y_boundary,'-'))
            bottom_right = (getRelativePos(current_x,resolution,y_boundary,'+'), getRelativePos(current_y,resolution,y_boundary,'-'))

            if(dist((current_x,current_y),top)!=np.inf):
                current_point = getPointFromPos(x_boundary,resolution,current_x,current_y)
                dist_point = getPointFromPos(x_boundary,resolution,top[0],top[1])
                if(current_point <= total_points and dist_point <= total_points):
                    if(warehouse_specification.intersects(Point(top))):
                        distance_matrix[current_point][dist_point] = dist((current_x,current_y),top)
                        
            if(dist((current_x,current_y),bottom)!=np.inf):
                current_point = getPointFromPos(x_boundary,resolution,current_x,current_y)
                dist_point = getPointFromPos(x_boundary,resolution,bottom[0],bottom[1])
                if(current_point <= total_points and dist_point <= total_points):
                    if(warehouse_specification.intersects(Point(bottom))):
                        distance_matrix[current_point][dist_point] = dist((current_x,current_y),bottom)
                        
            if(dist((current_x,current_y),left)!=np.inf):
                current_point = getPointFromPos(x_boundary,resolution,current_x,current_y)
                dist_point = getPointFromPos(x_boundary,resolution,left[0],left[1])
                if(current_point <= total_points and dist_point <= total_points):
                    if(warehouse_specification.intersects(Point(left))):
                        distance_matrix[current_point][dist_point] = dist((current_x,current_y),left)
                        
            if(dist((current_x,current_y),right)!=np.inf):
                current_point = getPointFromPos(x_boundary,resolution,current_x,current_y)
                dist_point = getPointFromPos(x_boundary,resolution,right[0],right[1])
                if(current_point <= total_points and dist_point <= total_points):
                    if(warehouse_specification.intersects(Point(right))):
                        distance_matrix[current_point][dist_point] = dist((current_x,current_y),right)
                        
            if(dist((current_x,current_y),top_left)!=np.inf):
                current_point = getPointFromPos(x_boundary,resolution,current_x,current_y)
                dist_point = getPointFromPos(x_boundary,resolution,top_left[0],top_left[1])
                if(current_point <= total_points and dist_point <= total_points):
                    if(warehouse_specification.intersects(Point(top_left))):
                        distance_matrix[current_point][dist_point] = dist((current_x,current_y),top_left)
                        
            if(dist((current_x,current_y),top_right)!=np.inf):
                current_point = getPointFromPos(x_boundary,resolution,current_x,current_y)
                dist_point = getPointFromPos(x_boundary,resolution,top_right[0],top_right[1])
                if(current_point <= total_points and dist_point <= total_points):
                    if(warehouse_specification.intersects(Point(top_right))):
                        distance_matrix[current_point][dist_point] = dist((current_x,current_y),top_right)
                        
            if(dist((current_x,current_y),bottom_left)!=np.inf):
                current_point = getPointFromPos(x_boundary,resolution,current_x,current_y)
                dist_point = getPointFromPos(x_boundary,resolution,bottom_left[0],bottom_left[1])
                if(current_point <= total_points and dist_point <= total_points):
                    if(warehouse_specification.intersects(Point(bottom_left))):
                        distance_matrix[current_point][dist_point] = dist((current_x,current_y),bottom_left)        
                        
                        
            if(dist((current_x,current_y),bottom_right)!=np.inf):
                current_point = getPointFromPos(x_boundary,resolution,current_x,current_y)
                dist_point = getPointFromPos(x_boundary,resolution,bottom_right[0],bottom_right[1])
                if(current_point <= total_points and dist_point <= total_points):
                    if(warehouse_specification.intersects(Point(bottom_right))):
                        distance_matrix[current_point][dist_point] = dist((current_x,current_y),bottom_right)

    
    # construct graph data
    data = {}
    for i in range(total_points):
        edges = {}
        for j in range(i,total_points):
            edges[j] = distance_matrix[i][j]
        data[i]=edges

    g = Graph(data=data,undirected=True)
    return g

def getPath(g, start_point, end_point, x_boundary,y_boundary,resolution):

    path = find_path(g,getPointFromPos(x_boundary,resolution,start_point[0],start_point[1]),getPointFromPos(x_boundary,resolution,end_point[0],end_point[1]))
    track = []
    for i in path.nodes:
        track.append((getPosFromPoint(x_boundary,resolution,i)[0],getPosFromPoint(x_boundary,resolution,i)[1]))
    # minimal the track
    curr_dir = 0
    prev_dir = 0
    prev_point = (0,0)
    steps = 1
    new_track = []
    for i in range(len(track)-1):
        diff = (track[i+1][0]-track[i][0],track[i+1][1]-track[i][1])
        if       (diff == ( resolution,0         )):
            curr_dir = 90
        elif (diff == ( resolution,resolution)):
            curr_dir = -135
        elif (diff == (0          ,resolution)):
            curr_dir = 180
        elif (diff == (-resolution,resolution)):
            curr_dir = 135
        elif (diff == ( -resolution,0          )):
            curr_dir = 90
        elif (diff == (-resolution,-resolution)):
            curr_dir = 45
        elif (diff == (0          ,-resolution)):
            curr_dir = 0
        elif (diff == (resolution,-resolution)):
            curr_dir = -45
            
        if(i==0):
                prev_dir = curr_dir;
                prev_point = track[i]
        else:
            if(prev_dir == curr_dir):
                steps = steps + 1;
                if( i == len(track)-2):
                        if(not(prev_dir % 10)):
                            new_track.append((prev_dir,  steps*resolution,prev_point,track[i+1]))
                        else:
                            new_track.append((prev_dir,  steps*resolution*sqrt(2),prev_point,track[i+1]))
            else:
                if(not(prev_dir % 10)):
                    new_track.append((prev_dir,  steps*resolution,prev_point,track[i]))
                else:
                    new_track.append((prev_dir,  steps*resolution*sqrt(2),prev_point,track[i] ))
                    
                steps = 1
                prev_dir = curr_dir
                prev_point = track[i]
                    
                if( i == len(track)-2):
                        if(not(prev_dir % 10)):
                            new_track.append((prev_dir,  steps*resolution,prev_point,track[i+1]))
                        else:
                            new_track.append((prev_dir,  steps*resolution*sqrt(2),prev_point,track[i+1]))
    return new_track

if __name__ == '__main__':
    '''
    warehouse_data =  [
        (100  , 100),
        (3100 , 100),
        (3100 , 2300),
        (1500 , 2300),
        (1500 , 2500),
        (3100 , 2500),
        (3100 , 4500),
        (100  , 4500),
        (100  , 2500),
        (900  , 2500),
        (900  , 2300),
        (100  , 2300),
        (100  , 100)]

    g = setGraph(warehouse_data,3500,5000,50)
'''
    g = Graph.load('/home/graph')
    path = getPath(g,(2100 ,3600),(400,800),3500,5000,50)


    print(path)

