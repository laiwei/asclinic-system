#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import rospy
from std_msgs.msg import UInt32
from std_msgs.msg import UInt16
from std_msgs.msg import Float32
# Import the asclinic message types
from asclinic_pkg.msg import PathInstruction

class myCarrierControl:

    def __init__(self):
        rospy.loginfo("This is carrier control")

        # Initialise a publisher
        self.set_servo_pulse_width = rospy.Publisher("/my_global_namespace"+"/set_servo_pulse_width", UInt16, queue_size=10)
        
        # Initialise a subscriber
        # rospy.Subscriber("/my_global_namespace"+"/set_instruction_done", UInt32, self.myInstructionDoneCallback)


        self.myCarrierControl()



    def myCarrierControl(self):
        rospy.sleep(5.0)
        self.set_servo_pulse_width.publish(1)
        rospy.sleep(5.0)
        self.set_servo_pulse_width.publish(2)
        rospy.sleep(5.0)
        self.set_servo_pulse_width.publish(0)




if __name__ == '__main__':
    # Initialise the node
    global node_name
    node_name = "my_carrier_control"
    rospy.init_node(node_name)
    template_py_node = myCarrierControl()
    # Spin as a single-threaded node
    rospy.spin()
