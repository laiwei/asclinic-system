#!/usr/bin/env python3
# -*- coding: utf-8 -*-


# Import numpy
import numpy as np

# Import opencv
import cv2

# Import aruco
import cv2.aruco as aruco

# DEFINE THE PARAMETERS
# > For the number of the USB camera device
#   i.e., for /dev/video0, this parameter should be 0
USB_CAMERA_DEVICE_NUMBER = 0

# > For the size of the aruco marker, in meters
MARKER_SIZE = 0.151

# > For where to save images captured by the camera
#   Note: ensure that this path already exists
#   Note: images are only saved when a message is received
#         on the "request_save_image" topic.
SAVE_IMAGE_PATH = "/home/asc13/saved_camera_images/"

# > A flag for whether to display the images captured
SHOULD_SHOW_IMAGES = False

camera_setup = USB_CAMERA_DEVICE_NUMBER

# Initialise video capture from the camera
cam=cv2.VideoCapture(camera_setup)

# Get the ArUco dictionary to use
aruco_dict = aruco.Dictionary_get(aruco.DICT_4X4_50)

# Create an parameter structure needed for the ArUco detection
aruco_parameters = aruco.DetectorParameters_create()
# > Specify the parameter for: corner refinement
aruco_parameters.cornerRefinementMethod = aruco.CORNER_REFINE_SUBPIX

intrinic_camera_matrix = np.array( [[1726,0,1107] , [0,1726,788] , [0,0,1]], dtype=float)
intrinic_camera_distortion  = np.array( [[ 5.5252e-02, -2.3523e-01, -1.0507e-04, -8.9834e-04, 2.4028e-01]], dtype=float)
return_flag , current_frame = cam.read()
if (return_flag == True):

    temp_filename = SAVE_IMAGE_PATH + "image_aruco_test_real.jpg"
    cv2.imwrite(temp_filename,current_frame)
    print('Saved!')

    current_frame_gray = cv2.cvtColor(current_frame, cv2.COLOR_BGR2GRAY)
    aruco_corners_of_all_markers, aruco_ids, aruco_rejected_img_points = aruco.detectMarkers(current_frame_gray, aruco_dict, parameters=aruco_parameters)
    if aruco_ids is not None:
        # print('Aruco Ids Detected!'+str(len(aruco_ids)))

         for i_marker_id in range(len(aruco_ids)):
            this_id = aruco_ids[i_marker_id]

            current_frame_with_marker_outlines = aruco.drawDetectedMarkers(current_frame.copy(), aruco_corners_of_all_markers, aruco_ids, borderColor=(0, 220, 0))

            # this_id = aruco_ids[1]
            corners_of_this_marker = aruco_corners_of_all_markers[i_marker_id]
            this_rvec_estimate, this_tvec_estimate, _objPoints = aruco.estimatePoseSingleMarkers(corners_of_this_marker, MARKER_SIZE, intrinic_camera_matrix, intrinic_camera_distortion)
            
            # print(this_tvec_estimate)

            rvec = this_rvec_estimate[0]
            tvec = this_tvec_estimate[0]
            current_frame_with_marker_outlines = aruco.drawAxis(current_frame_with_marker_outlines, intrinic_camera_matrix, intrinic_camera_distortion, rvec, tvec, MARKER_SIZE)
            rvec = rvec[0]
            tvec = tvec[0]
            Rmat = cv2.Rodrigues(rvec)

            # print("[TEMPLATE ARUCO DETECTOR] for id = " + str(this_id) + ", tvec = [ " + str(tvec[0]) + " , " + str(tvec[1]) + " , " + str(tvec[2]) + " ]" )
            # print(Rmat[0])

            # print(aruco_ids)

            # 已知
            Rmtc = Rmat[0]
            Tctm_c = np.array([tvec]).T
            Rctm = np.linalg.inv(Rmtc)
            Tctm_m = Rctm.dot(Tctm_c)
            id = this_id[0]

            # 查表
            Rmtw = [
                [],
                np.array([[-1,0,0],[0,1,0],[0,0,-1]]), #1
                np.array([[0,0,1],[0,1,0],[-1,0,0]]),
                np.array([[0,0,1],[0,1,0],[-1,0,0]]),
                np.array([[0,0,1],[0,1,0],[-1,0,0]]),
                np.array([[1,0,0],[0,1,0],[0,0,1]]), #5
                np.array([[1,0,0],[0,1,0],[0,0,1]]),
                np.array([[1,0,0],[0,1,0],[0,0,1]]),
                np.array([[1,0,0],[0,1,0],[0,0,1]]),
                np.array([[0,0,-1],[0,1,0],[1,0,0]]),
                np.array([[0,0,-1],[0,1,0],[1,0,0]]), #10
                np.array([[0,0,-1],[0,1,0],[1,0,0]]),
                np.array([[-1,0,0],[0,1,0],[0,0,-1]]), 
                np.array([[-1,0,0],[0,1,0],[0,0,-1]]), 
                np.array([[1,0,0],[0,1,0],[0,0,1]]),
                np.array([[1,0,0],[0,1,0],[0,0,1]]), #15
                np.array([[1,0,0],[0,1,0],[0,0,1]]),
            ]
            Tmtw_w = [
                [],
                np.array([[-0.4],[0],[-2.38]]), #1
                np.array([[0],[0],[-2.0]]),
                np.array([[0],[0],[-1.2]]),
                np.array([[0],[0],[-0.4]]),
                np.array([[-0.4],[0],[0]]),    #5
                np.array([[-1.2],[0],[0]]),
                np.array([[-2.0],[0],[0]]),
                np.array([[-2.8],[0],[0]]),
                np.array([[-3.2],[0],[-0.4]]),
                np.array([[-3.2],[0],[-1.2]]), #10
                np.array([[-3.2],[0],[-2.0]]),
                np.array([[-2.8],[0],[-2.38]]),
                np.array([[-2.0],[0],[-2.38]]),
                np.array([[-0.4],[0],[-2.4]]),
                np.array([[-2.0],[0],[-2.4]]), #15
                np.array([[-2.8],[0],[-2.4]]),
            ]

            # 计算
            Rw = Rmtw[id]
            # print(Rw,Tctm_m)

            Tctm_w = Rw.dot(Tctm_m)
            Tm = Tmtw_w[id]
            # print(tvec)

            T = -Tm - Tctm_w
            
            x = T[0]
            y = T[2]


            theta = 0

            if(id>=2 and id <= 4):
                if (rvec/np.pi*180)[1]>0:
                    theta = 270 - (rvec/np.pi*180)[1]
                else:
                    theta = -90-(rvec/np.pi*180)[1]

            elif((id>=5 and id <= 8) or (id>=14 and id <= 16)):
                if (rvec/np.pi*180)[1]>0:
                    theta = 180 - (rvec/np.pi*180)[1]
                else:
                    theta = -(rvec/np.pi*180)[1]-180

            elif(id == 1 or id == 12 or id == 13):
                theta = -(rvec/np.pi*180)[1]

            if(id>=9 and id <= 11):
                if ((rvec/np.pi*180)[1] > 0):
                    theta = 90 - (rvec/np.pi*180)[1] 
                else:
                    theta = -(rvec/np.pi*180)[1]-270


            # print(id,int(x*1000//50*50),int(y*1000//50*50),theta)

            print("The Detected Marker ID is ", id)
            # print("The corresponding rvec is ", (rvec/np.pi*180)[1], "in degree")
            print("The current position is ", int(x*1000//50*50),int(y*1000//50*50), "in mm")
            print("The current angle is ", theta, "in degree")    


        


        # Tctm_c = np.array([tvec]).T
        # Rmtc = Rmat[0]
        
        # Rctm = np.linalg.inv(Rmtc)
        # Tctm_m = Rctm.dot(Tctm_c)
        # res = -(Tctm_m+np.array([[1],[0],[0]]))
        # print(theta)
        # print(tvec)
        # print(str(aruco_ids)+"current position: (",str(-res[0][0]),", "+str(res[2][0])+")")
