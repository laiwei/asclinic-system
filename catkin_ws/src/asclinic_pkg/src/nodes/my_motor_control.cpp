#include "ros/ros.h"
#include <ros/package.h>

#include "std_msgs/UInt32.h"
#include "std_msgs/Float32.h"

// Include the asclinic message types
#include "asclinic_pkg/MotorDriveControl.h"
#include "asclinic_pkg/LeftAndRightInt.h"
#include "asclinic_pkg/PathInstruction.h"
// Namespacing the package
using namespace asclinic_pkg;

float left_wheel_dis = 0.0;
float right_wheel_dis = 0.0;

int target_theta = -1;
int target_distance = 0;

float prev_theta = 0.0;

bool clock_wise_rotate = true;


ros::Publisher m_template_publisher;
ros::Publisher my_reset_count_publisher;
ros::Publisher my_instruction_done;

std_msgs::UInt32 my_reset_count_publisher_msg;


void setMortorTurnClockwise(int speed){
	MotorDriveControl driver_msg;
	driver_msg.dir = 0; // 转向
	driver_msg.speed = speed; // 速度20%
	m_template_publisher.publish(driver_msg);
}

void setMortorTurnAntiClockwise(int speed){
	MotorDriveControl driver_msg;
	driver_msg.dir = 1; // 转向
	driver_msg.speed = speed; // 速度20%
	m_template_publisher.publish(driver_msg);
}

void setMortorBackward(int speed){
	MotorDriveControl driver_msg;
	driver_msg.dir = 3; // 直行
	driver_msg.speed = speed; // 速度20%
	m_template_publisher.publish(driver_msg);
}


void setMortorForward(int speed){
	MotorDriveControl driver_msg;
	driver_msg.dir = 2; // 直行
	driver_msg.speed = speed; // 速度20%
	m_template_publisher.publish(driver_msg);
}



void setMortorStop(){
	MotorDriveControl driver_msg;
	driver_msg.dir = 1; // 直行
	driver_msg.speed = 0; // 速度20%
	m_template_publisher.publish(driver_msg);
}


void templateSubscriberCallback(const LeftAndRightInt & msg)
{

	left_wheel_dis = msg.left;
	right_wheel_dis = msg.right;
	// ROS_INFO_STREAM("Right Wheel Distance"<< right_wheel_dis<<"Left Wheel Distance"<< left_wheel_dis);
	my_reset_count_publisher_msg.data = 0;	
	// 判断当前指令是否完成
	if(target_theta != -1){
		
		float theta = ((left_wheel_dis+right_wheel_dis)/218)/3.14*180;

		int current_theta = 0;
		if (clock_wise_rotate){
			current_theta = (int (theta + prev_theta))%360;
		} else {
			current_theta = (int (prev_theta - theta + 720))%360;
		}

		// ROS_INFO_STREAM("Robot Theta"<< current_theta <<"prev"<<prev_theta<<"current"<<theta);
		// ROS_INFO_STREAM("Robot Target theta "<< (target_theta - 1) << "->" <<(target_theta + 1));

		if(  current_theta >= (target_theta - 1) &&
			 current_theta <= (target_theta + 1)
		  ){
			// 到达目标角度
			setMortorStop();
			ros::Duration(0.5).sleep();
			my_reset_count_publisher.publish(my_reset_count_publisher_msg);
			my_instruction_done.publish(my_reset_count_publisher_msg);
			target_theta = -1;
			prev_theta = current_theta;
		}
	}
	if(target_distance){

		int current_distance = (left_wheel_dis + right_wheel_dis)/2;

		if( current_distance >= (target_distance-3) && 
			current_distance <= (target_distance+3)){
			// 到达目标距离
			setMortorStop();
			ros::Duration(0.5).sleep();
			my_reset_count_publisher.publish(my_reset_count_publisher_msg);
			my_instruction_done.publish(my_reset_count_publisher_msg);
			target_distance = 0;
		}


	}

}

void SetCurrentThetaCallBack(const std_msgs::Float32 &msg ){
	prev_theta = msg.data;
	my_reset_count_publisher_msg.data = 0;	
	my_reset_count_publisher.publish(my_reset_count_publisher_msg);
	ROS_INFO_STREAM("Reset Theta to: "<< prev_theta);
	
}

void getPathInstructionCallBack(const PathInstruction & msg){
	// ROS_INFO_STREAM("Robot Theta: "<< int (msg.theta)<<",Robot Dis: "<<msg.dis);

	int isRelative = msg.isRelative;
	int theta = msg.theta;
	int dis = msg.dis;

	if(theta != -1){

		if(isRelative){
			target_theta = int (prev_theta + theta + 360) % 360;
		} else {
			target_theta = (theta + 360) % 360;
		}
		// ROS_INFO_STREAM("Target "<< target_theta<<"Prev "<< prev_theta);

		int diff_theta = int(target_theta - prev_theta + 360) % 360;
		if(diff_theta < 180){
			clock_wise_rotate = true;
			setMortorTurnClockwise(20);

		} else {
			clock_wise_rotate = false;
			setMortorTurnAntiClockwise(20);
		}
	}
	else if(dis){
		if(dis > 0){
			target_distance = dis;
			setMortorForward(20);
		} else {
			target_distance = -dis;
			setMortorBackward(20);
		}
	} else {
		setMortorTurnClockwise(0);
		my_reset_count_publisher_msg.data = 0;
		my_reset_count_publisher.publish(my_reset_count_publisher_msg);
		my_instruction_done.publish(my_reset_count_publisher_msg);
	}
}



int main(int argc, char* argv[])
{
	// Initialise the node
	ros::init(argc, argv, "my_motor_control");
	ros::NodeHandle node_handle_for_global("/my_global_namespace");

	// Initialise a publisher
	m_template_publisher = node_handle_for_global.advertise<MotorDriveControl>("set_motor_duty_cycle", 10, false);
	my_reset_count_publisher = node_handle_for_global.advertise<std_msgs::UInt32>("reset_counts",10,false);
	my_instruction_done = node_handle_for_global.advertise<std_msgs::UInt32>("set_instruction_done",10,false);


	ros::Subscriber encoder_counts_subscriber = node_handle_for_global.subscribe("encoder_counts", 1, templateSubscriberCallback);
	ros::Subscriber get_path_instruction_subscriber = node_handle_for_global.subscribe("get_path_instruction", 1, getPathInstructionCallBack);
	ros::Subscriber set_current_theta = node_handle_for_global.subscribe("set_current_theta",1, SetCurrentThetaCallBack);

	ros::spin();

	return 0;
}
