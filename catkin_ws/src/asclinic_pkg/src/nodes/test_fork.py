#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import rospy
from std_msgs.msg import UInt16

class Test:

    def __init__(self):
        # Initialise a publisher
        self.template_publisher = rospy.Publisher("/my_global_namespace"+"/set_servo_pulse_width", UInt16, queue_size=10)
        rospy.sleep(2.0)
        print('run')
        self.runMotor()



    # Respond to timer callback
    def runMotor(self):
        msg = UInt16()
        # msg.data = 1
        # self.template_publisher.publish(msg)
        # print('publish')
        # rospy.sleep(2)
        # msg.data = 0
        # self.template_publisher.publish(msg)
        # rospy.sleep(1.0)
        msg.data = 2
        self.template_publisher.publish(msg)
        rospy.sleep(1.5)
        msg.data = 0
        self.template_publisher.publish(msg)


if __name__ == '__main__':
    # Initialise the node
    global node_name
    node_name = "Test"
    rospy.init_node(node_name)
    template_py_node = Test()
    # Spin as a single-threaded node
    rospy.spin()
