.. _software-installation:

SOFTWARE INSTALLATION
=====================

.. toctree::
  :maxdepth: 2
  :hidden:

  software_installation_manual
  software_installation_script


Before proceeding with the installation, ensure that you are familiar with:

* :ref:`command line basics <linux-cmd-line>` for a Linux operating system.
* :ref:`git <git>` for software versioning control.

The installation procedure for the :code:`asclinic-system` is detailed in:

* :ref:`Manual software installation <software-installation-manual>`
* :ref:`Software installation script <software-installation-script>`
